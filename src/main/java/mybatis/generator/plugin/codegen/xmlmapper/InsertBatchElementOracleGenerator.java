package mybatis.generator.plugin.codegen.xmlmapper;

import mybatis.generator.plugin.common.Common;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.ListUtilities;
import org.mybatis.generator.codegen.mybatis3.MyBatis3FormattingUtilities;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.elements.AbstractXmlElementGenerator;
import org.mybatis.generator.internal.util.StringUtility;

import java.util.List;

/**
 * @Description: oracle 批量插入
 * @Author: wanghanrui
 */
public class InsertBatchElementOracleGenerator extends AbstractXmlElementGenerator {
    @Override
    public void addElements(XmlElement parentElement) {
        XmlElement insertElement = new XmlElement("insert");
        insertElement.addAttribute(new Attribute("id", Common.INSERTBATCH));

        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        sb.append("insert into ");
        sb.append(introspectedTable.getFullyQualifiedTableNameAtRuntime());
        sb.append(" (");
        sb1.append("( <foreach collection=\"list\" item=\"item\" index=\"index\" separator=\"union all\"> SELECT ");
        List<IntrospectedColumn> columns = ListUtilities.removeIdentityAndGeneratedAlwaysColumns(introspectedTable.getAllColumns());
        for (int i = 0; i < columns.size(); i++) {
            IntrospectedColumn introspectedColumn = columns.get(i);

            sb.append(MyBatis3FormattingUtilities
                    .getEscapedColumnName(introspectedColumn));

            sb1.append(getParameterClause(introspectedColumn, "item", null));
            if (i + 1 < columns.size()) {
                sb.append(", ");
                sb1.append(", ");
            }
        }
        sb.append(") ");
        sb1.append("from DUAL");
        sb1.append("</foreach> ) ");
        insertElement.addElement(new TextElement(sb.toString()));

        insertElement.addElement(new TextElement(sb1.toString()));
        parentElement.addElement(insertElement);

    }

    public static String getParameterClause(IntrospectedColumn introspectedColumn, String item, String prefix) {
        StringBuilder sb = new StringBuilder();
        sb.append("#{");
        sb.append(item + '.' + introspectedColumn.getJavaProperty(prefix));
        sb.append(",jdbcType=");
        sb.append(introspectedColumn.getJdbcTypeName());
        if (StringUtility.stringHasValue(introspectedColumn.getTypeHandler())) {
            sb.append(",typeHandler=");
            sb.append(introspectedColumn.getTypeHandler());
        }

        sb.append('}');
        return sb.toString();
    }
}
