package mybatis.generator.plugin.common;

/**
 * @Description:TODO
 * @Author: wanghanrui
 * @DateTime: 2020/6/27 22:34
 */
public class Common {

    public static final String SELECTONE = "selectOne";
    public static final String SELECTLIST = "selectList";
    public static final String PAGELIST = "pageList";
    public static final String INSERTBATCH = "insertBatch";
    public static final String DELETEBATCHIDS = "deleteBatchIds";
    public static final String SELECTBATCHIDS = "selectBatchIds";
}
